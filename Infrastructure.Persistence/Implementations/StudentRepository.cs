﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Entities;
using Infrastructure.Persistence.DTO;
using Infrastructure.Persistence.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Persistence.Implementations
{
    public class StudentRepository : IStudentRepository
    {
        private readonly SchoolDbContext _context;
        private readonly IMemoryCache _cache;
        private readonly IMapper _mapper;

        private const int GetAllKey = 0;

        public StudentRepository(SchoolDbContext context, IMapper mapper, IMemoryCache cache)
        {
            _context = context;
            _mapper = mapper;
            _cache = cache;
        }
        
        public StudentDto GetById(int id)
        {
            if (_cache.TryGetValue(id, out StudentDto studentDto)) return studentDto;
            
            var student = _context.Students.Find(id);
            if (student == null) return null;
                
            studentDto = _mapper.Map<StudentDto>(student);
            _cache.Set(student.Id, studentDto, 
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(1)));


            return studentDto;
        }

        public IList<StudentDto> GetAll()
        {
            if (_cache.TryGetValue(GetAllKey, out IList<StudentDto> studentsDto)) return studentsDto;
            
            var allStudents = _context.Students.ToList();
            if (!allStudents.Any()) return studentsDto;
            
            studentsDto = _mapper.Map<List<StudentDto>>(allStudents);
            _cache.Set(GetAllKey, studentsDto, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(1)));

            return studentsDto;
        }

        public StudentDto Create(StudentDto studentDto)
        {
            var student = _mapper.Map<Student>(studentDto);

            var savedStudentEntry = _context.Students.Add(student);
            var result = _context.SaveChanges();

            var createdStudentDto = _mapper.Map<StudentDto>(savedStudentEntry.Entity);

            if (result > 0)
            {
                _cache.Set(createdStudentDto.Id, createdStudentDto,
                    new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(1)));
                //_cache.Remove(GetAllKey);
            }

            return createdStudentDto;
        }

        public StudentDto Update(int id, StudentDto studentDto)
        {
            var student = _context.Students.Find(id);

            student.Name = studentDto.Name;

            //_context.Entry(student).State = EntityState.Modified;
            _context.Students.Update(student);
            _context.SaveChanges();

            return _mapper.Map<StudentDto>(student);
        }

        public StudentDto Remove(int id)
        {
            var student = _context.Students.Find(id);
            _context.Students.Remove(student);
            _context.SaveChanges();

            return _mapper.Map<StudentDto>(student);
        }
    }
}