﻿using Infrastructure.Persistence.DTO;
using MediatR;
using School.Responses;

namespace School.Commands
{
    public class CreateStudentCommand : IRequest<StudentResponse>
    {
        public StudentDto StudentDto { get; }

        public CreateStudentCommand(StudentDto studentDto)
        {
            StudentDto = studentDto;
        }
    }
}