﻿using Infrastructure.Persistence.DTO;
using MediatR;

namespace School.Commands
{
    public class UpdateStudentCommand : IRequest<StudentDto>
    {
        public StudentDto StudentDto { get; }
        public int Id { get; set; }

        public UpdateStudentCommand(int id, StudentDto studentDto)
        {
            Id = id;
            StudentDto = studentDto;
        }
    }
}