﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Persistence.DTO;
using Infrastructure.Persistence.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using School.Commands;
using School.Responses;

namespace School.RequestHandlers
{
    public class CreateStudentRequestHandler : IRequestHandler<CreateStudentCommand, StudentResponse>
    {
        private readonly IStudentRepository _studentRepository;
        private readonly ILogger<CreateStudentRequestHandler> _logger;

        public CreateStudentRequestHandler(IStudentRepository studentRepository, ILogger<CreateStudentRequestHandler> logger)
        {
            _studentRepository = studentRepository;
            _logger = logger;
        }

        public async Task<StudentResponse> Handle(CreateStudentCommand request, CancellationToken cancellationToken)
        {

            var result = _studentRepository.Create(request.StudentDto);
            var response = new StudentResponse();

            if (result == null)
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.ErrorMessage = "Not created";

                _logger.LogError("Not created");
            }
            else
            {
                response.StatusCode = HttpStatusCode.OK;
                response.StudentDto = result;
            }

            
            return response;
        }
    }
}