﻿using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Persistence.DTO;
using Infrastructure.Persistence.Interfaces;
using MediatR;
using School.Commands;

namespace School.RequestHandlers
{
    public class UpdateStudentRequestHandler : IRequestHandler<UpdateStudentCommand, StudentDto>
    {
        private readonly IStudentRepository _studentRepository;

        public UpdateStudentRequestHandler(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        public async Task<StudentDto> Handle(UpdateStudentCommand request, CancellationToken cancellationToken)
        {

            var result = _studentRepository.Update(request.Id, request.StudentDto);

            return result;
        }
    }
}